<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    $nama = 'Siska Khusnul Khotimah';
    return view('about', ['nama' => $nama]);
});

Route::get('/experiences', function () {
    // get data from a database
    $experiences = [
      ['type' => 'Computer Science Lecturer ', 'base' => '2023'],
      ['type' => 'UI/UX Designer Lead ', 'base' => '2023'],
      ['type' => 'UI/UX Designer', 'base' => '2022'],
      ['type' => 'Software Enggineer ', 'base' => '2022'],

    ];
    return view('experiences', ['experiences' => $experiences]);

  });